Source: golang-github-sigstore-rekor
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Reinhard Tartler <siretart@tauware.de>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-asaskevich-govalidator-dev,
               golang-github-blang-semver-dev,
               golang-github-go-chi-chi-dev,
               golang-github-go-openapi-errors-dev,
               golang-github-go-openapi-loads-dev,
               golang-github-go-openapi-runtime-dev,
               golang-github-go-openapi-spec-dev,
               golang-github-go-openapi-strfmt-dev,
               golang-github-go-openapi-swag-dev,
               golang-github-go-openapi-validate-dev,
               golang-github-golang-mock-dev,
               golang-github-google-go-cmp-dev,
               golang-github-hashicorp-go-cleanhttp-dev,
               golang-github-hashicorp-go-retryablehttp-dev,
               golang-github-jedisct1-go-minisign-dev,
               golang-github-mitchellh-go-homedir-dev,
               golang-github-mitchellh-mapstructure-dev,
               golang-github-rs-cors-dev,
               golang-github-secure-systems-lab-go-securesystemslib-dev,
               golang-github-sigstore-sigstore-dev,
               golang-github-spf13-cobra-dev,
               golang-github-spf13-pflag-dev,
               golang-github-spf13-viper-dev,
               golang-github-theupdateframework-go-tuf-dev,
               golang-go.uber-zap-dev,
               golang-gocloud-dev,
               golang-golang-x-crypto-dev,
               golang-golang-x-exp-dev,
               golang-golang-x-mod-dev,
               golang-golang-x-net-dev,
               golang-golang-x-sync-dev,
               golang-google-genproto-dev,
               golang-google-grpc-dev,
               golang-google-protobuf-dev,
               golang-gopkg-ini.v1-dev,
               golang-k8s-sigs-yaml-dev,
               golang-prometheus-client-dev,
               golang-uber-goleak-dev,
               golang-yaml.v2-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-sigstore-rekor
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-sigstore-rekor.git
Homepage: https://sigstore.dev
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/sigstore/rekor

Package: golang-github-sigstore-rekor-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-asaskevich-govalidator-dev,
         golang-github-blang-semver-dev,
         golang-github-go-chi-chi-dev,
         golang-github-go-openapi-errors-dev,
         golang-github-go-openapi-loads-dev,
         golang-github-go-openapi-runtime-dev,
         golang-github-go-openapi-spec-dev,
         golang-github-go-openapi-strfmt-dev,
         golang-github-go-openapi-swag-dev,
         golang-github-go-openapi-validate-dev,
         golang-github-golang-mock-dev,
         golang-github-google-go-cmp-dev,
         golang-github-hashicorp-go-cleanhttp-dev,
         golang-github-hashicorp-go-retryablehttp-dev,
         golang-github-jedisct1-go-minisign-dev,
         golang-github-mitchellh-go-homedir-dev,
         golang-github-mitchellh-mapstructure-dev,
         golang-github-rs-cors-dev,
         golang-github-secure-systems-lab-go-securesystemslib-dev,
         golang-github-sigstore-sigstore-dev,
         golang-github-spf13-cobra-dev,
         golang-github-spf13-pflag-dev,
         golang-github-spf13-viper-dev,
         golang-github-theupdateframework-go-tuf-dev,
         golang-go.uber-zap-dev,
         golang-gocloud-dev,
         golang-golang-x-crypto-dev,
         golang-golang-x-exp-dev,
         golang-golang-x-mod-dev,
         golang-golang-x-net-dev,
         golang-golang-x-sync-dev,
         golang-google-genproto-dev,
         golang-google-grpc-dev,
         golang-google-protobuf-dev,
         golang-gopkg-ini.v1-dev,
         golang-k8s-sigs-yaml-dev,
         golang-prometheus-client-dev,
         golang-uber-goleak-dev,
         golang-yaml.v2-dev,
         ${misc:Depends}
Description: Software Supply Chain Transparency Log (library)
 Rekor's goals are to provide an immutable tamper resistant ledger of
 metadata generated within a software projects supply chain. Rekor will
 enable software maintainers and build systems to record signed metadata
 to an immutable record. Other parties can then query said metadata to
 enable them to make informed decisions on trust and non-repudiation of an
 object's lifecycle.
 .
 The Rekor project provides a restful API based server for validation and
 a transparency log for storage. A CLI application is available to make
 and verify entries, query the transparency log for inclusion proof,
 integrity verification of the transparency log or retrieval of entries
 by either public key or artifact.
 .
 Rekor fulfils the signature transparency role of sigstore's software
 signing infrastructure. However, Rekor can be run on its own and is
 designed to be extensible to working with different manifest schemas and
 PKI tooling.

Package: rekor
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Software Supply Chain Transparency Log (program)
 Rekor's goals are to provide an immutable tamper resistant ledger of
 metadata generated within a software projects supply chain. Rekor will
 enable software maintainers and build systems to record signed metadata
 to an immutable record. Other parties can then query said metadata to
 enable them to make informed decisions on trust and non-repudiation of an
 object's lifecycle.
 .
 The Rekor project provides a restful API based server for validation and
 a transparency log for storage. A CLI application is available to make
 and verify entries, query the transparency log for inclusion proof,
 integrity verification of the transparency log or retrieval of entries
 by either public key or artifact.
 .
 Rekor fulfils the signature transparency role of sigstore's software
 signing infrastructure. However, Rekor can be run on its own and is
 designed to be extensible to working with different manifest schemas and
 PKI tooling.
